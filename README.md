# i3 wm

### Installing

```
sudo apt install polybar
```

Prepare config file

```
mkdir ~/.config/polybar 
```
```
sudo cp /usr/share/doc/polybar/config ~/.config/polybar 
```

```
ls -al .config/polybar/                          
```

total 20
drwxr-xr-x  2 mparmac mparmac  4096 Apr 18 15:04 .
drwxr-xr-x 47 mparmac mparmac  4096 Apr 18 15:03 ..
-rw-r--r--  1 mparmac mparmac 10945 Apr 18 15:04 config

```
sudo chown mparmac:mparmac .config/polybar/config 
```

Launch script 
```
nano .config/polybar/launch.sh 
```

```
#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch bar1 and bar2
echo "---" | tee -a /tmp/polybar1.log /tmp/polybar2.log
polybar bar1 2>&1 | tee -a /tmp/polybar1.log & disown
polybar bar2 2>&1 | tee -a /tmp/polybar2.log & disown

echo "Bars launched..."
```


i3 config
nano .config/i3/config

```
# polybar launch script
exec_always --no-startup-id $HOME/.config/polybar/launch.sh
```